import React,{useState,createContext} from 'react';



export const StateProvider = createContext()

const State = props => {
    const [api,setApi] = useState('App')
    return ( 
        <StateProvider.Provider value={[api,setApi]}>
            {props.children}
        </StateProvider.Provider>
    );
}
 
export default State;