import React, { useContext,useState } from "react";
import Button from "./Components/Button/Button.component";
import Card from "./Components/Card/Card.component";
import Search from "./Components/Search/Search.component";
import Section from "./Components/Section/Section.component";
import Layout from "./Infrastructure/Layout/Layout";
import  { StateProvider } from "./State Management/State";
import { color } from "./Infrastructure/theme/color";
import Daily from './Components/Daily Card/Daily.component.js'
const App = () => {
  const [api] = useContext(StateProvider);

    
  const [toggle,setToggle]  = useState('5 Days')


    const Toggle=(e)=>{
      setToggle(e)
    }
  return (
    <>
    
        <Layout>

          <Search/>
          <Section  alignItems={'flex-end'}>
          <Card alignItems={'center'} width={'40%'}>
            <h1>Icon</h1>

          <h3>Abuja</h3>
          <Section marginTop={'12px'} padding={'0px'} flexDirection={'row'} alignItems={'space-evenly'} >
            <Section marginTop={'0px'} width={'40%'} padding={'0px'}>
              <h2>7C</h2>
            </Section>
            <Section marginTop={'0px'} padding={'0'} width={'60%'} flexDirection={'row'}>
              <Section flexBasis={'50%'} marginTop={'0px'} padding={'0px'} flexDirection={'column'}>
                  <label>Min</label>
                  <p>23</p>
              </Section>
              <Section flexBasis={'50%'} marginTop={'0px'} padding={'0'} flexDirection={'column'}>
                <label>Max</label>
                <p>34</p>
              </Section>
            </Section>
          </Section>
          </Card>
          <Section width={'40%'}  flexDirection={'row'} >
            <Button background={'transparent'} border={`1.5px solid ${color.primary_color}`} width={'35%'} onClick={()=>Toggle('Today')}  >Today</Button>
            <Button background={color.secondary_color} border={`1.5px solid ${color.primary_color}`} textcolor={color.primary_color} width={'35%'} onClick={()=>Toggle('5 Days')}  >5 Days</Button>
          </Section>
          </Section>
        
              {toggle === 'Today'  &&          <h1>Today</h1>
        }


        {toggle === '5 Days' &&           <Section padding={'0px'} flexDirection={'row'} justifyContent={'start'} alignItems={'flex-start'}>
           <Daily day={'Monday'} date={'05.12.2020'} icon={"rain"} min={'0.5'}  max={'7.0'}/>
           <Daily day={'Monday'} date={'05.12.2020'} icon={"rain"} min={'0.5'}  max={'7.0'}/>
           <Daily day={'Monday'} date={'05.12.2020'} icon={"rain"} min={'0.5'}  max={'7.0'}/>
           <Daily day={'Monday'} date={'05.12.2020'} icon={"rain"} min={'0.5'}  max={'7.0'}/>
          </Section>
         }
        </Layout>
   
    </>
  );
 
};

export default App;
