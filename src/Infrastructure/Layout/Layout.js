import styled from "styled-components";
import Background from "../../Assets/background.jpg"
const LayoutStyle=styled.div`
min-height:100vh;
width:100vw;
display:flex;
flex-direction:column;
align-items:center;
// justify-content:space-between;
background-image:${({background})=> background ? background : `url(${Background})`};
background-repeat:no-repeat;
background-size:cover;
background-position:center;
`


const Layout = ({children}) => {
    return ( 
        <>
        <LayoutStyle>{children}</LayoutStyle>
        </>
     );
}
 
export default Layout;