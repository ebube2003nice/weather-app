import React from 'react';
import styled from "styled-components"
import { color } from '../../Infrastructure/theme/color';


const ButtonStyles = styled.button`
background: ${({background})=>background ? background : color.primary_color};
color: ${({textcolor})=>textcolor ? textcolor : color.secondary_color};
padding: ${({padding})=>padding ? padding : "15px"};
min-width:50px;
width:${({width})=>width ? width : "70px"};
border: ${({border})=>border ? border : 'none'};
border-radius: ${({bdr})=>bdr ? bdr : "50px"};
outline:none;
cursor:pointer;
/**&:hover{
    background:${({hover})=>hover ? hover : color.primary_color}
};**/

&:focus{
    /**background:${({hover})=>hover ? hover : color.primary_color};**/
    background:white;
    color:${color.primary_color}
};
@media only screen and (max-width:670px){
    width:30%;
    padding:8px;
}
@media only screen and (max-width:270px){
    width:20%;
}


`
const Button = (
    {children,
    onClick,
    padding,
    width,
    background,
    textcolor,
    border,
    bdr,
    hover

}
) => {
    return (

        <>
        <ButtonStyles  hover={hover} bdr={bdr} border={border} background={background} textcolor={textcolor} padding={padding} width={width} onClick={onClick}  >{children}</ButtonStyles>
        </>
      );
}
 
export default Button;