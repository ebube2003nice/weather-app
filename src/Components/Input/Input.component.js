import React from 'react'; 
import styled from 'styled-components';
import { color } from '../../Infrastructure/theme/color';

const InputStyles = styled.input`
text:${({textcolor})=>textcolor ? textcolor : 'white'};
padding:${({padding})=>padding ? padding : '3px'};
width:${({width})=>width ? width : '53px'};
border: ${({border})=>border ? border : 'none'};
border-radius: ${({bdr})=>bdr ? bdr : "50px"};
outline:none;
height:100%;
background: ${({background})=>background ? background : color.primary_color};
@media only screen and (max-width:270px){
    width:50%;
}
`

const Input = ({value,type,onChange,width,padding,textcolor,border,bdr,background}) => {
    return ( 
        <>
        <InputStyles background={background} textcolor={textcolor} padding={padding} border={border} bdr={bdr}  width={width} value={value} onChange={onChange} type={type} />
        </>
        );
}
 
export default Input;