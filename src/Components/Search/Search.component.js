

import styled from "styled-components";
import { color } from "../../Infrastructure/theme/color";
import Button from "../Button/Button.component";
import Input from "../Input/Input.component";

const SearchStyles = styled.div`
width:50%;
margin-top:30px;
display:flex;
justify-contents:center;
align-items:center;
border-radius:5px;
background:${color.secondary_color};

@media only screen and (max-width:670px){
    width:80%;
    padding:15px;
}
@media only screen and (max-width:470px){
    width:90%;
}
`

const Search=()=>{
    return(
        <>
        <SearchStyles>
            <Input background={'transparent'} padding={'15px'} bdr={'none'} width={'70%'}/>
            <Button  bdr={'none'} width={'30%'} >Search</Button>
        </SearchStyles>
        </>
    )
}

export default Search