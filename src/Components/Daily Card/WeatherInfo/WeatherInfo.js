import React from 'react';
import './WeatherInfo.css';
const WeatherInfo = ({name,info}) => {
    return ( 
        <div className="contentContainer">
        <div className="content">
            <p>{name}</p>
            <p>{info}</p>
        </div>
        </div>
     );
}
 
export default WeatherInfo;