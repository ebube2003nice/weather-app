import { useState } from "react";
import styled from "styled-components";
import { color } from "../../Infrastructure/theme/color";
const DailyStyles = styled.div`

display:flex;
align-items:center;
justify-content:space-evenly;
flex-direction:column;
flex-basis:10%;
color:white;
font-size:12px;
background:${color.primary_color}
`

const  Daily = ({day,date,icon,min,max}) => {
    return ( 
        <>
        <DailyStyles>
            <h3>{day}</h3>
            <h4>{date}</h4>
            <h5>{icon}</h5>
            <p>min:{min}</p>
            <p>max:{max}</p>
        </DailyStyles>
        </>
     );
}
 
export default Daily;

