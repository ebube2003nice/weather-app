import React from 'react'; 
import styled from 'styled-components';
import { color } from '../../Infrastructure/theme/color';

const CardStyles = styled.div`
color:${({textcolor})=>textcolor ? textcolor : 'white'};
padding:${({padding})=>padding ? padding : '23px'};
cursor:pointer;
width:${({width})=>width ? width : ''};
display:flex;
flex-direction:${({flexDirection})=>flexDirection ? flexDirection : 'column'};
align-items:${({alignItems})=>alignItems ? alignItems : 'center'};
justify-contents:${({justifyContent})=>justifyContent ? justifyContent : 'space-evenly'};
background:${color.primary_color};
@media only screen and (max-width:670px){
    width:80%;
    padding:0px;
}

`

const Card = ({children, width,flexDirection,alignItems,justifyContents}) => {
    return ( 
        <CardStyles justifyContents={justifyContents} flexDirection={flexDirection} alignItems={alignItems} width={width}>{children}</  CardStyles>
     );
}
 
export default Card;