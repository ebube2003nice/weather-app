import styled from "styled-components";
import { color } from "../../Infrastructure/theme/color";

const Sectionstyles = styled.section`

width:${({width})=>width ? width : '90%'};
margin-top:${({marginTop})=>marginTop ? marginTop : '23px'};
/*min-height:50vh;*/
padding:${({padding})=>padding ? padding : '23px'};
display:flex;
flex-direction:${({flexDirection})=>flexDirection ? flexDirection : 'column'};
align-items:${({alignItems})=>alignItems ? alignItems : 'center'};

justify-content:${({justifyContent})=>justifyContent ? justifyContent : 'space-evenly'};
flex-wrap:wrap;
flex-basis:${({flexBasis})=>flexBasis ? flexBasis : 'auto'};

@media only screen and (max-width:670px){
    width:80%;
    align-items:center;
    justify-content:center;
}
`


const   Section=({children, width, alignItems,flexDirection,padding,marginTop,flexBasis,justifyContent})=>{
    return(
        <>
        <Sectionstyles justifyContent={justifyContent}  flexBasis={flexBasis} marginTop={marginTop} width={width} padding={padding} flexDirection={flexDirection} alignItems={alignItems} >{children}</Sectionstyles>
        </>
    )
}


export default Section